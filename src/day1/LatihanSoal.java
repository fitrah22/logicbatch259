package day1;

import java.util.Scanner;

public class LatihanSoal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		System.out.print("Pulsa : ");
		
		int pulsa = input.nextInt();
		int point = 0;
		
		if (pulsa >= 10000 && pulsa < 25000) {
			point = 80;
		} else if (pulsa >= 25000 && pulsa < 50000) {
			point = 200;
		} else if (pulsa >= 50000 && pulsa < 100000) {
			point = 400;
		} else if (pulsa >= 100000) {
			point = 800;
		} else {
			point = 0;
		}
		
		System.out.println("Point : +" + point);
		
	}

}
