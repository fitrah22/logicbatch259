package day1;

import java.util.Scanner;

public class Logic {
	int id = 1;
	String nama = "Fitrah";

	int a = 10;
	int b = 20;
	
	boolean salah = false;
	
	int arr[] = {10,20,30};
	
	public static void main(String[] args) {
		
		Logic l = new Logic();
		
		System.out.println("Nama : " + l.nama);
		System.out.println(l.a+l.b);
		System.out.println(l.salah);
		
		double seribu = 1000.0;
		double duaribu = 2000.0;
		
		System.out.println(seribu+duaribu);
		
		// cara mengubah String menjadi Integer
		String strA = "5";
		String strB = "8";
		
		System.out.println(Integer.parseInt(strA) + Integer.parseInt(strB));
		System.out.println();
		
		// untuk menginput data
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Nilai 1 : " );
		int numberOne = input.nextInt();

		System.out.println("Masukkan Nilai 2 : " );
		int numberTwo = input.nextInt();
		
		int jumlah = numberOne + numberTwo;
		
//		System.out.println("Jumlah dari numberOne dan numberTwo = " + (numberOne+numberTwo));
		System.out.println("Hasil : " + jumlah);
		
		
	}

}
