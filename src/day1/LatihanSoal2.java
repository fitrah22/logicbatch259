package day1;

import java.util.Scanner;

public class LatihanSoal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.print("Belanja : ");
		int belanja = input.nextInt();
		
		System.out.print("Jarak : ");
		int jarak = input.nextInt();

		System.out.print("Masukkan Promo = ");
		String kode = input.next();
		
		int ongkir = 0;
		int diskon = 0;
		
//		diskon = (belanja * 40/100);	
//		int total = belanja + ongkir - diskon;
		
		if (kode.equals("JKTOVO")) {
			if (belanja >= 30000) {
				diskon = belanja * 40/100;
			} else if (belanja < 30000){
				diskon = 0;
			}
		} if (diskon > 30000) {
			diskon = 30000;
		}
		if (jarak > 5) {
			ongkir = jarak * 1000;
		} else if (jarak <= 5) {
			ongkir = 5000;
		}
		
		System.out.println("---------------------");
		System.out.println("Belanja = " + belanja);
		System.out.println("Diskon 40% : " + diskon);
		System.out.println("Ongkir : " + ongkir);
		System.out.println("Total Belanja : " + (belanja + ongkir - diskon));
	}

}
