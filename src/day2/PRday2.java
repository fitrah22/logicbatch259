package day2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class PRday2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		// Soal 1
//		System.out.println("Masukkan Uang Andi : ");
//		int uangAndi = input.nextInt();
//		
//		System.out.println("Masukkan Daftar Baju : ");
//		int[] daftarBaju = new int[4];
//		for (int i = 0; i < daftarBaju.length; i++) {
//			int hargaBaju = input.nextInt();
//			daftarBaju[i] = hargaBaju;
//		}
//
//		System.out.println("Masukkan Daftar Celana : ");
//		int[] daftarCelana = new int[4];
//		for (int i = 0; i < daftarCelana.length; i++) {
//			int hargaCelana = input.nextInt();
//			daftarCelana[i] = hargaCelana;
//		}
//
//		int x = 0;
//		int max = 0;	
//		
//		for (int i = 0; i < daftarBaju.length; i++) {
//			x = daftarBaju[i] + daftarCelana[i];
//			if (x <= uangAndi && x > max) {
//				max = x;
//			}
//			System.out.println(max);
//		}  

		// Soal 2
//		System.out.print("Masukkan angka : ");
//		int n = input.nextInt();
//
//		int[] arr = new int[n];
//		System.out.println("arr : ");
//		for (int i = 0; i < n; i++) {
//			int angka = input.nextInt();
//			arr[i] = angka;
//		}
//
//		System.out.print("rot : ");
//		int rotasi = input.nextInt();
//
//		int[] x = new int[rotasi];
//		for (int i = 0; i < rotasi; i++) {
//			x[i] = arr[0];
//			for (int j = 0; j < arr.length - 1; j++) {
//				arr[j] = arr[j + 1];
//				System.out.print(x[j]);
//			}
//			arr[arr.length - 1] = x[i];
//			System.out.println(x[i]);
//		}

		// Soal 3
//		System.out.println("Jumlah puntung rokok yang dipungut : ");
//			int n = input.nextInt();
//			
//			int x = n / 8;
//			int y = (n - (x*8));
//			int uang = x * 500;
//			
//			System.out.println("Batang yang dapat dirangkai = " + x + " batang");
//			System.out.println("Sisa puntung rokok " + y);
//			System.out.println("Penghasilan = " + uang);

		// Soal 4

//		System.out.println("Total Menu : ");
//		int n = input.nextInt();
//
//		System.out.println("Makan alergi index ke - : ");
//		int alergi = input.nextInt();
//
//		System.out.println("Harga Menu : ");
//		int[] hargaMenu = new int[n];
//
//		for (int i = 0; i < hargaMenu.length; i++) {
//			int menu = input.nextInt();
//			hargaMenu[i] = menu;
//		}
//
//		System.out.println("Uang Elsa : ");
//		int uangElsa = input.nextInt();
//
//		int total = 0;
//		for (int i = 0; i < hargaMenu.length; i++) {
//			total = total + hargaMenu[i];
//		}
//
//		int jumlah = total - hargaMenu[alergi];
//		int makanElsa = jumlah / 2;
//		int sisaUang = uangElsa - makanElsa;
//
//		System.out.println("total makanan yang dimakan elsa dan dimas : " + total);
//		System.out.println("makanan yang dimakan elsa : " + jumlah);
//		System.out.println("\n Elsa harus membayar : " + makanElsa);
//
//		if (sisaUang == 0) {
//			System.out.println("Uang Pas" + sisaUang);
//		} else if (sisaUang < makanElsa) {
//			System.out.println("Elsa kurang membayar : " + (sisaUang * (-1)));
//		} else {
//			System.out.println("sisa uang Elsa : " + sisaUang);
//		}

		// Soal 5
//		System.out.println("Masukkan Kalimat : ");

		// toLowerCase untuk mengubah hufur kapital menjadi kecil
		// replace untuk mengganti 1 karakter dengan karakter yang lain
//		String kalimat = input.nextLine().toLowerCase().replaceAll(" ", "");
//
//		char[] huruf = kalimat.toCharArray();
//		String vokal = "";
//		String konsonan = "";
//		String urutan = "";
//		Arrays.sort(huruf);
//
//		for (int i = 0; i < huruf.length; i++) {
//			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o') {
//				vokal += huruf[i];
//			} else {
//				konsonan += huruf[i];
//			}
//
//		}
//
//		System.out.println();
//		System.out.println("Huruf Vokal : " + vokal);
//		System.out.println("Konsonan : " + konsonan);
//		System.out.println(Arrays.toString(huruf));
		
		// Soal 6
		System.out.print("Masukkan kalimat : ");
		String kata = input.nextLine();

		String result = "";
		String[] tempSplit = kata.split(" ");

		for (int i = 0; i < tempSplit.length; i++) {
			char[] arr = tempSplit[i].toCharArray();
			for (int j = 0; j < arr.length; j++) {
				if (i % 2 == 1) {
					result += "*";
				} else {
					result += arr[j];
				}
			}
			result += " ";
		}
		System.out.println(result);
		
	}
}
