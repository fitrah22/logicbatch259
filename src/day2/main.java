package day2;

import java.util.Scanner;

public class main {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println(("Enter the number of case"));
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					case01.Resolve1();
					break;
				case 2:
					case01.Resolve2();
					break;
				case 3:
					case01.Resolve3();
					break;
				case 4:
					case01.Resolve4();
					break;
				case 5:
					case01.Resolve5();
					break;
				case 6:
					case01.Resolve6();
					break;
				case 7:
					case01.Resolve7();
					break;
				case 8:
					case01.Resolve8();
					break;
				case 9:
					case01.Resolve9();
					break;
				case 10:
					case01.Resolve10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}

			System.out.println();
			System.out.println("Continue ?");
			answer = input.nextLine();

		}
	}
}