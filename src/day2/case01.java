package day2;

import java.util.Scanner;

public class case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		System.out.print("Masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		System.out.println("Soal 1");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x *= n2;
					System.out.print(tempArray[i][j] + " ");
				}

			}
			System.out.println();
		}
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		System.out.print("Masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		int x = 1;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 3 == 0) {
					tempArray[i][j] = x;
					x = (x * n2) * (1);
					System.out.print(tempArray[i][j] + " ");
				} else {
					tempArray[i][j] = x;
					x = (x) * (-n2);
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve3() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		System.out.print("Masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int[][] tempArray = new int[2][n];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j >= 3) {
					tempArray[i][j] = n2;
					System.out.print(tempArray[i][j] + " ");
					n2 /= 2;
				} else {
					tempArray[i][j] = n2;
					n2 *= 2;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		System.out.print("Masukkan nilai n2 = ");
		int n2 = input.nextInt();

		int x = 1;
		int[][] tempArray = new int[2][n];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (j % 2 == 1) {
					tempArray[i][j] = n2;
					System.out.print(tempArray[i][j] + " ");
					n2 += n2;
				} else {
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
					x++;
				}
			}
			System.out.println();
		}
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int x = 0;
		int[][] tempArray = new int[2][n];

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
	}

	public static void Resolve6() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int x = 1;
		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x *= n;
					System.out.print(tempArray[i][j] + " ");
				} else {
					x = tempArray[0][j] + tempArray[1][j];
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve7() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int x = 0;
		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				tempArray[i][j] = x;
				System.out.print(tempArray[i][j] + " ");
				x += 1;
			}
			System.out.println();
		}
	}

	public static void Resolve8() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan nilai n : ");
		int n = input.nextInt();

		int x = 0;
		int[][] tempArray = new int[3][n];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					tempArray[i][j] = j;
					System.out.print(tempArray[i][j] + " ");
				} else if (i == 1) {
					tempArray[i][j] = x;
					x += 2;
					System.out.print(tempArray[i][j] + " ");
				} else {
					x = tempArray[0][j] + tempArray[1][j];
					tempArray[i][j] = x;
					System.out.print(tempArray[i][j] + " ");
				}
			}
			System.out.println();
		}
	}

	public static void Resolve9() {

	}

	public static void Resolve10() {

	}
}