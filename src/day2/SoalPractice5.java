package day2;

import java.util.Scanner;

public class SoalPractice5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan Nilai n : ");
		int n = input.nextInt();

		int x = 1;

		int[] tempInteger = new int[n];

		for (int i = 0; i < tempInteger.length; i++) {
			if (i % 3 == 2) {
				System.out.print("* ");
			} else {
				tempInteger[i] = x;
				System.out.print(tempInteger[i] + " ");
				x += 4;
			}
		}

	}

}
