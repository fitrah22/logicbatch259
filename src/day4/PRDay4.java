package day4;

import java.util.Iterator;
import java.util.Scanner;

public class PRDay4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Soal 1
//		System.out.println("Masukkan Jumlah Customer yang akan dituju : ");
//		int n = input.nextInt();
//
//		double jumlah = 0;
//		double bensin = 0;
//
//		double customer[] = new double[5];
//		customer[1] = 2;
//		customer[2] = 0.5;
//		customer[3] = 1.5;
//		customer[4] = 0.3;
//
//		System.out.println("Customer yang akan dituju : ");
//
//		for (int i = 0; i < n; i++) {
//			int jarak = input.nextInt();
//			jumlah = jumlah + customer[jarak];
//		}
//		System.out.println("Jarak Tempuh = " + jumlah);
//
//		bensin = jumlah / 2.5;
//		
//		System.out.println("Bensin = " + Math.ceil(bensin) + " liter");
		
		
	// cara dengan dinamis
//		int tempJarak[] = new int[4];
//		double total = 0;
//		double bensin = 0;
//		
//		System.out.println("Masukkan Jarak : ");
//		for (int i = 0; i < tempJarak.length; i++) {
//			int jarak = input.nextInt();
//			tempJarak[i] = jarak;
//		}
//		
//		input.nextLine(); // untuk menginput setelah di enter atau spasi
//		
//		System.out.println("Masukkan Index Jarak : ");
//		String inputIndexSplit[] = input.nextLine().split(" "); // split untuk menghilangkan spasi
//		
//		for (int i = 0; i < inputIndexSplit.length; i++) {
//			total += tempJarak[Integer.parseInt(inputIndexSplit[i])-1]; // angka -1 untuk menyamakan output dari tempJarak
//		}
//
//		bensin = total / 2500;
//		
//		System.out.println(total);
//		System.out.println(bensin);
		

	// Soal 2
		System.out.println("Masukkan Sinyal SOS : ");
		String s = input.nextLine().toUpperCase();

		char[] huruf = s.toCharArray();
		int count = 0;

		for (int i = 0; i < s.length(); i += 3) {
			if (huruf[i] != 'S' || huruf[i + 1] != 'O' || huruf[i + 2] != 'S') {
				count++;
			}
		}
		System.out.println(count);
		
		
		
	}

}
