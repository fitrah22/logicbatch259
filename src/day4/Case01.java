package day4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Case01 {
	public static void Resolve1() {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Kalimat : ");
		String s = input.nextLine();

		int word = 0;
		for (int i = 0; i < s.length(); i++) {
			if (Character.isUpperCase(s.charAt(i))) {
				word++;
			}
		}
		System.out.println(word + 1);
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);
		System.out.println("\n\nSTRONG PASSWORD");
		String s = input.nextLine();

		char[] huruf = s.toCharArray();
		int number = 0;
		int upper = 0;
		int lower = 0;
		int special = 0;
		int count = 0;

		for (int i = 0; i < s.length(); i++) {
			if (huruf[i] >= 48 && huruf[i] <= 57) { // ASCII 48 = 0 && ASCII 57 = 9
				number++;
			} else if (huruf[i] >= 65 && huruf[i] <= 90) { // ASCII 65 = A && ASCII = Z
				upper++;
			} else if (huruf[i] >= 97 && huruf[i] <= 122) { // ASCII 97 = a && ASCII 122 = z
				lower++;
			} else {
				special++;
			}
		}
		if (number == 0) {
			count++;
		}
		if (upper == 0) {
			count++;
		}
		if (lower == 0) {
			count++;
		}
		if (special == 0) {
			count++;
		}

		if (s.length() >= 6) {
			System.out.println(count);
		} else {
			System.out.println(count + (6 - (s.length() + count)));
		}
	}

	public static void Resolve3() {
		Scanner input = new Scanner(System.in);
		int x = input.nextInt();
		String code = input.next();
		int s = input.nextInt();

		char[] tempArr = code.toCharArray();
		String result = "";

		for (int i = 0; i < tempArr.length; i++) {
			if (Character.isUpperCase(tempArr[i])) {
				char ch = (char) (((int) tempArr[i] + s - 65) % 26 + 65);
				result += ch;
			} else {
				char ch = (char) (((int) tempArr[i] + s - 97) % 26 + 97);
				result += ch;
			}
		}
		System.out.println("Text	: " + code);
		System.out.println("Shift	: " + s);
		System.out.println("Cipher	: " + result);
	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Sinyal SOS : ");
		String s = input.nextLine();

		char[] huruf = s.toCharArray();
		int count = 0;

		for (int i = 0; i < s.length(); i += 3) {
			if (huruf[i] != 'S' || huruf[i + 1] != 'O' || huruf[i + 2] != 'S') {
				count++;
			}
		}
		System.out.println(count);
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);
		int q = input.nextInt();
		while (q-- > 0) {
			String s1 = input.next();
			String s2 = "hack";
			int m = s1.length();
			int n = s2.length();

			char a[] = s1.toCharArray();
			char b[] = s2.toCharArray();

			int c[][] = new int[n + 1][m + 1];

			for (int i = 1; i <= n; i++) {
				for (int j = 1; j <= m; j++) {
					if (b[i - 1] == a[j - 1]) {
						c[i][j] = c[i - 1][j - 1] + 1;
					} else {
						c[i][j] = Math.max(c[i - 1][j], c[i][j - 1]);
					}
				}
			}

			int result = c[n][m];
			if (result == n) {
				System.out.println("YES");
			} else {
				System.out.println("NO");
			}
		}
	}

	public static void Resolve6() {
		Scanner input = new Scanner(System.in);
		System.out.print("Masukan kalimat: ");
		String kalimat = input.nextLine().replaceAll(" ", "").toLowerCase();

		String hasil = "";

		for (char i = 'a'; i <= 'z'; i++) {
			if (kalimat.indexOf(i) != -1) {
				hasil += i;
			}
		}

		if (hasil.length() == 26) {
			System.out.println("Pangram");
		} else {
			System.out.println("Bukan Pangram");
		}
	}

	public static void Resolve7() {
		Scanner input = new Scanner(System.in);

//		String s = input.nextLine();
//		String subString = "";
//		boolean isValid = false;
//		for (int i = 0; i < s.length(); i++) {
//			subString = s.substring(0, i);
//			Long num = Long.parseLong(subString);
//			String validString = subString;
//			while (validString.length() < s.length()) {
//				validString += Long.toString(++num);
//			}
//			if (s.equals(validString)) {
//				isValid = true;
//				break;
//			}
//		}
//		System.out.println(isValid ? "YES" + subString : "NO");
	}

	public static void Resolve8() {
		Scanner input = new Scanner(System.in);
		int numberM = input.nextInt();

		String inputChar = "";
		int[] count = new int[26];
		int[] temp;

		for (int i = 0; i < numberM; i++) {
			temp = new int[26];
			inputChar = input.next();
			for (int c : inputChar.toCharArray()) {
				temp[c - 97]++;
				if (temp[c - 97] == 1)
					count[c - 97]++;
			}
			temp = null;
		}
		int sum = 0;
		for (int i = 0; i < 26; i++) {
			if (count[i] == numberM)
				sum += 1;
		}
		count = null;
		System.out.println(sum);
	}

	public static void Resolve9() {
		Scanner input = new Scanner(System.in);
		String string1 = input.next();
		String string2 = input.next();

		int hasil = 0;

		char[] arr1 = new char[string1.length()];
		char[] arr2 = new char[string2.length()];

		for (int i = 0; i < arr1.length; i++) {
			arr1[i] = string1.charAt(i);
		}

		for (int i = 0; i < arr2.length; i++) {
			arr2[1] = string2.charAt(i);
		}

		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr2.length; j++) {
				if (arr1[i] == arr2[j]) {
					hasil++;
					arr2[j] = '0';
					break;
				}
			}

		}
		System.out.println((string1.length() + string2.length()) - (2 * hasil));

	}

	public static void Resolve10() {
		Scanner input = new Scanner(System.in);
		Set<Character> s1Set = new HashSet<>();
		Set<Character> s2Set = new HashSet<>();

		System.out.print("Masukan kata pertama : ");
		String s1 = input.nextLine();
		for (char ch : s1.toCharArray()) {
			s1Set.add(ch);
		}
		System.out.print("Masukan kata kedua : ");
		String s2 = input.nextLine();
		for (char ch : s2.toCharArray()) {
			s2Set.add(ch);
		}
		s1Set.retainAll(s2Set);
		if (!s1Set.isEmpty()) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}
}
