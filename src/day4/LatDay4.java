package day4;

import java.text.*;
import java.util.*;

public class LatDay4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Soal 1
		// Mican masuk ke gedung satrio pada 20/8/2019 jam 07.50. lalu ia keluar gedung
		// pada pukul 17.30
		// dihari yang sama. Berapa ia harus membayar parkir jika biaya parkir 1 jam
		// adalah 3000. perhitngan di
		// bulatkan keatas jika durasi 2:30 berarti 3 jam jadi membayar parkir 9000

//		System.out.println("Jam Masuk : ");
//		String masuk = input.next();
//		String jamMasuk = masuk.substring(0, 2);
//		String menitMasuk = masuk.substring(3);
//		
//		System.out.println("Keluar Gedung pada Jam : ");
//		String keluar = input.next();
//		String jamKeluar = keluar.substring(0, 2);
//		String menitKeluar = keluar.substring(3);
//		
//		int jamMasukR = Integer.parseInt(jamMasuk);
//		int menitMasukR = Integer.parseInt(menitMasuk);
//		int jamKeluarR = Integer.parseInt(jamKeluar);
//		int menitKeluarR = Integer.parseInt(menitKeluar);
//		
//		int time = (3600 * jamMasukR) + (60 * menitMasukR);
//		int timeK = (3600 * jamKeluarR) + (60 * menitKeluarR);
//	
//		int detik = time - timeK;
//		int jammm = detik / 3600;
//		int sisa = detik % 3600;
//		int menit = sisa / 60;
//		
//		System.out.println("Lama Parkiran = " + jammm + " jam" + menit + " menit");
//		
//		if(menit > 30) {
//			jammm+=1;
//		}
//		int tarif = jammm * 3000;
//		System.out.println(tarif);

//		System.out.println("Jam Masuk :");
//		String masuk = input.nextLine();
//		System.out.println("\nJam Keluar : ");
//		String keluar = input.nextLine();
//		
//		SimpleDateFormat minjam = new SimpleDateFormat("dd-MM-yyyy HH:mm");
//		
//		Date m1 = null;
//		Date k1 = null;
//		
//		try {
//			m1 = minjam.parse(masuk);
//			k1 = minjam.parse(keluar);
//			long selisih = k1.getTime() - m1.getTime();
//			long selisihJam = selisih / (60 * 60 * 1000);
//			long bayar = selisihJam * 3000;
//			System.out.println("\nBayar Parkir = Rp. " + bayar);
//		} catch (ParseException p) {	
//			p.printStackTrace();
//		}

		// Soal 2
		// Mono Meminjam buku algorima dasar pada tanggal 09-06-2019 ia harus
		// mengembalikan lagi setelah 3 hari.
		// Namun mono telat mengembalikan bukunya sampai tanggal 10-07-2019, berapakan
		// mono harus membayar denda.
		// Apabila telat dikenakan 500 dalam satu hari

//		System.out.println("Tanggal Peminjaman :");
//		String pinjam = input.nextLine();
//		
//		System.out.println("Hari Pengembalian : ");
//		int hari = input.nextInt();
//		
//		input.nextLine();
//		
//		System.out.println("\nTanggal Pengembalian : ");
//		String kembali = input.nextLine();
//		
//		SimpleDateFormat minjam = new SimpleDateFormat("dd-MM-yyyy");
//	
//		try {
//			Date peminjaman = minjam.parse(pinjam);
//			Date pengembalian = minjam.parse(kembali);
//			long selisih = pengembalian.getTime() - peminjaman.getTime();
//			long selisihTanggal = selisih / (24 * 60 * 60 * 1000);
//			int bilBulat = (int) selisihTanggal;
//			
//			if (selisihTanggal > hari) {
//			long telat = selisihTanggal - hari;
//			long denda = telat * 500;
//			System.out.println("\nDenda = Rp. " + denda);
//			}
//		} catch (ParseException p) {	
//			p.printStackTrace();
//		}

		// Soal 3
		System.out.println("masukkan tanggal masuk : ");
		String inputTanggalMulai = input.nextLine();

		System.out.println("masukkan lama bootcamp : ");
		int lama = input.nextInt();
		input.nextLine();

		System.out.println("masukkan tanggal libur : ");
		String[] libur = input.nextLine().split(",");

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

		String nextDate = "";
		try {

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(dateFormat.parse(inputTanggalMulai));

			int hitung = 0;
			int tempHitungLibur = 0;

			int i = 0;
			while (i < lama + tempHitungLibur) {
				calendar.add(Calendar.DATE, 1);
				int day = calendar.getTime().getDay();
				int dateofHoliday = calendar.getTime().getDate();

				int j = 0;
				if ((day == 6) || (day == 0)) {
					tempHitungLibur++;
				}
				while (j < libur.length) {
					if (dateofHoliday == Integer.parseInt(libur[j])) {
						tempHitungLibur++;
					}
					j++;
				}
				i++;
			}

			System.out.println("total libur = " + (tempHitungLibur + libur.length));
			System.out.println(hitung);
			nextDate = dateFormat.format(calendar.getTime());
			System.out.println(nextDate);

		} catch (ParseException x) {
			x.printStackTrace();
		}

		// Soal 4
//		System.out.println("Input : ");
//		String k = input.nextLine();
//		
//		int panjang = k.toCharArray().length;
//		char kalimat[] = k.toCharArray();
//		String balik = "";
//		for (int i =panjang -  1; i >= 0 ; i--) {
//			balik += kalimat[i];
//		}	if (k.equals(balik)) {
//			System.out.println("\nYES");
//		} else {
//			System.out.println("\nNO");
//		}
//		System.out.println(balik);

	}

}
