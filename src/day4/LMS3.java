package day4;

//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Arrays;
//import java.util.Date;
import java.util.Scanner;

public class LMS3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Soal 1 done
//		int a = 2;
//		int b = 3;
//		
//		System.out.println(a + " + " + b + " = " + (a+b));

		// Soal 2 done
//		System.out.print("Input Waktu : ");
//		String waktu = input.nextLine().toLowerCase();
//
//		DateFormat awal = new SimpleDateFormat("hh:mm:ssaa");
//		DateFormat akhir = new SimpleDateFormat("HH:mm:ss");
//
//		try {
//			Date date = awal.parse(waktu);
//			String out = akhir.format(date);
//			System.out.println(out);
//		} catch (java.text.ParseException p) {
//			p.printStackTrace();
//		}

		// Soal 3 done
//		System.out.print("Input : ");
//		int n = input.nextInt();
//
//		int tempAngka[] = new int[n];
//		int jumlah = 0;
//		System.out.print("Masukkan Angka : ");
//		for (int i = 0; i < n; i++) {
//			int sum = input.nextInt();
//			tempAngka[i] = sum;
//			jumlah += tempAngka[i];
//		}
//		System.out.println(jumlah);

		// Soal 4 done
//		System.out.print("Input : ");
//		int n = input.nextInt();
//
//		int diagonalKanan = 0;
//		int diagonalKiri = 0;
//		int sum = 0;
//
//		int tempDiagonal[][] = new int[n][n];
//		System.out.println("Masukkan Bilangan Matrix : ");
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				int b = input.nextInt();
//				tempDiagonal[i][j] = b;
//			}
//		}
//
//		for (int i = 0; i < n; i++) {
//			for (int j = 0; j < n; j++) {
//				if (i == j) 
//					diagonalKiri += tempDiagonal[i][j];
//				if (i + j == n - 1) 
//					diagonalKanan += tempDiagonal[i][j];
//			}
//		}
//		
//		sum = diagonalKiri - diagonalKanan;
//		
//		System.out.println("Jumlah Diagonal Kanan : " + diagonalKanan);
//		System.out.println("Jumlah Diagonal Kiri : " + diagonalKiri);
//		System.out.println("Hasil SUM : " + sum*(-1));

		// Soal 5 done
//		System.out.println("Input : ");
//		double n = input.nextDouble();
//
//		double nilaiPositif = 0;
//		double nilai0 = 0;
//		double nilaiNegatif = 0;
//		
//		double arrayBilangan[] = new double[n];
//
//		System.out.println("Masukkan Angka : ");
//		for (int i = 0; i < arrayBilangan.length; i++) {
//			double in = input.nextDouble();
//			arrayBilangan[i] = in;
//		}
//		
//		for (int i = 0; i < arrayBilangan.length; i++) {
//				if (arrayBilangan[i] > 0) {
//					nilaiPositif++;
//				} 
//				else if (arrayBilangan[i] == 0) {
//					nilai0++;
//				} 
//				else if (arrayBilangan[i] < 0) {
//					nilaiNegatif++;
//				}
//			}
//
//		double avgPositif = nilaiPositif / n;
//		double avgNegatif = nilaiNegatif / n;
//		double avg0 = nilai0 / n;
//		
//		System.out.println("\nBilangan positif = " + avgPositif);
//		System.out.println("Peluang 0 = " + avg0);
//		System.out.println("peluan negatif = " + avgNegatif);

		// Soal 6 done
//		System.out.print("Input : ");
//		int n = input.nextInt();
//
//		for (int i = 1; i <= n; i++) {
//			for (int j = 5; j >= i; j--) {
//				System.out.print(" ");
//			}
//			for (int k = 1; k <= i; k++) {
//				System.out.print("#");
//			}
//			System.out.println();
//		}

		// Soal 7 done
//		int tempAngka[] = new int[5];
//	
//
//		System.out.println("Input : ");
//		for (int i = 0; i < 5; i++) {
//			tempAngka[i] = input.nextInt();
//		}
//		int min = tempAngka[0];
//		int max = 0;
//		int totalSum = 0;
//		for (int i = 0; i <  5; i++) {
//			totalSum += tempAngka[i];
//			if (tempAngka[i] < min) {
//				min = tempAngka[i];
//			}
//			else if (tempAngka[i] > max) {
//				max = tempAngka[i];
//			}
//		}
//		System.out.println((totalSum - max) + " " +  (totalSum - min));

		// Soal 8 done
//		System.out.println("Input :");
//		int n = input.nextInt();
//
//		int candles[] = new int[n];
//		int maxCandles = 0;
//		int lilin = 0;
//
//		System.out.println("Input Candles : ");
//		for (int i = 0; i < candles.length; i++) {
//			candles[i] = input.nextInt();
//		}
//		for (int i = 0; i < candles.length; i++) {
//			if (maxCandles < candles[i]) {
//				maxCandles = candles[i];
//			}
//		}
//		for (int j = 0; j < candles.length; j++) {
//			if (maxCandles == candles[j]) {
//				lilin++;
//			}
//		}
//		System.out.println(lilin);

		// Soal 9 done
//		System.out.println("Input : ");
//		int n =  input.nextInt();
//
//		long tempArray[] = new long[n];
//		long sum = 0;
//		
//		System.out.println();
//		for (int i = 0; i < tempArray.length; i++) {
//			tempArray[i] = input.nextLong();
//		}
//		
//		for (int i = 0; i < tempArray.length; i++) {
//			sum += tempArray[i];
//		}
//		System.out.println(sum);

		// Soal 10 done
//		System.out.println("Input : ");
//		int n = input.nextInt();
//
//		int aliceArr[] = new int[n];
//		int bobArr[] = new int[n];
//		int alice = 0;
//		int bob = 0;
//
//		System.out.println();
//		for (int i = 0; i < n; i++) {
//			aliceArr[i] = input.nextInt();
//		}
//		for (int i = 0; i < bobArr.length; i++) {
//			bobArr[i] = input.nextInt();
//		}
//
//		for (int i = 0; i < n; i++) {
//			if (aliceArr[i] > bobArr[i]) {
//				alice++;
//			} else if (bobArr[i] > aliceArr[i]) {
//				bob++;
//			}
//		}
//		System.out.println(alice + " " + bob);

	}
}
