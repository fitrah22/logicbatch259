package day4;

import java.util.Scanner;

public class LMS4 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
	// Soal 1
//		System.out.println("Masukkan Kalimat : ");
//		String s = input.nextLine();
//		
//		int word = 0;
//		for (int i = 0; i < s.length(); i++) {
//			if(Character.isUpperCase(s.charAt(i))) {
//				word++;
//			}
//		}
//		System.out.println(word+1);
		
	// Soal 2 done
//		int n = input.nextInt();
//		String s = input.nextLine();
//		
//		char[] huruf = s.toCharArray();
//		int number = 0;
//		int upper = 0;
//		int lower = 0;
//		int special = 0; 
//		int count = 0;
//		
//		for (int i = 0; i < s.length(); i++) {
//			if (huruf[i] >= 48 && huruf[i] <= 57) {  // ASCII 48 = 0 && ASCII 57 = 9
//				number++;
//			} else if (huruf[i] >= 65 && huruf[i] <= 90) { // ASCII 65 = A && ASCII = Z
//				upper++;
//			} else if (huruf[i] >= 97 && huruf[i] <= 122) { // ASCII 97 = a && ASCII 122 = z
//				lower++;
//			} else {
//				special++;
//			}	
//		}
//		if (number == 0) {
//			count++;
//		} 
//		if (upper == 0) {
//			count++;
//		}
//		if (lower == 0) {
//			count++;
//		}
//		if (special == 0) {
//			count++;
//		}	
//		
//		if(s.length() >= 6) {
//			System.out.println(count);
//		} else {
//			System.out.println(count + (6-(s.length()+count)));
//		}
		
	// Soal 3
//		int x = input.nextInt();
//		String code = input.next();
//		int s = input.nextInt();
//
//		char[] tempArr = code.toCharArray();
//		String result = "";
//
//		for (int i = 0; i < tempArr.length; i++) {
//			if (Character.isUpperCase(tempArr[i])) {
//				char ch = (char) (((int) tempArr[i] + s - 65) % 26 + 65);
//				result += ch;
//			} else {
//				char ch = (char) (((int) tempArr[i] + s - 97) % 26 + 97);
//				result += ch;
//			}
//		}
//		System.out.println("Text	: " + code);
//        System.out.println("Shift	: " + s);
//        System.out.println("Cipher	: " + result);
		
	// Soal 4 done
//		System.out.println("Masukkan Sinyal SOS : ");
//		String s = input.nextLine();
//		
//		char[] huruf = s.toCharArray();
//		int count = 0;
//		
//		for (int i = 0; i < s.length(); i+=3) {
//			if (huruf[i] != 'S' || huruf[i + 1] != 'O' || huruf[i + 2] != 'S') {
//				count++;
//			}
//		}
//		System.out.println(count);
		
	// Soal 5 
//		   int q = input.nextInt();
//	        while (q-- > 0) {
//	            String s1 = input.next();
//	            String s2 = "hack";
//	            int m = s1.length();
//	            int n = s2.length();
//
//	            char a[] = s1.toCharArray();
//	            char b[] = s2.toCharArray();
//
//	            int c[][] = new int[n + 1][m + 1];
//
//	            for (int i = 1; i <= n; i++) {
//	                for (int j = 1; j <= m; j++) {
//	                    if (b[i - 1] == a[j - 1]) {
//	                        c[i][j] = c[i - 1][j - 1] + 1;
//	                    } else {
//	                        c[i][j] = Math.max(c[i - 1][j], c[i][j - 1]);
//	                    }
//	                }
//	            }
//
//	            int result = c[n][m];
//	            if(result == n) {
//	                System.out.println("YES");
//	            } else {
//	                System.out.println("NO");
//	            }
//	        }
		
		
		
	}
}
