package PretestLogic;

import java.util.*;

public class PretestLogic {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Soal 1 ganjil dan genap
//		System.out.print("Input : ");
//		int n = input.nextInt();
//
//		for (int i = 1; i <= n; i++) {
//			if (i % 2 == 1) {
//				System.out.print(i + " ");
//			}
//		}
//		System.out.println();
//		
//		for (int j = 1; j <= n; j++) {
//			if (j % 2 == 0) {
//				System.out.print(j + " ");
//			}
//		}

//		// Soal 2 huruf vokal
//		System.out.println("Masukkan Kalimat : ");
//		String kalimat = input.nextLine().toLowerCase().replaceAll(" ", "");
//
//		char[] huruf = kalimat.toCharArray();
//		String vokal = "";
//		String konsonan = "";
//		Arrays.sort(huruf);
//
//		for (int i = 0; i < huruf.length; i++) {
//			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o') {
//				vokal += huruf[i];
//			} else {
//				konsonan += huruf[i];
//			}
//		}
//
//		System.out.println();
//		System.out.println("Huruf Vokal : " + vokal);
//		System.out.println("Konsonan : " + konsonan);
//		System.out.println(Arrays.toString(huruf));

		// Soal 3
//		System.out.print("Input : ");
//		int n = input.nextInt();
//		
//		int awal = 99;
//		int tambah = 1;
//		
//		for (int i = 0; i < n; i++) {
//			awal += tambah;
//			System.out.println(awal + " adalah " + "'Si Angka 1'");
//			tambah = tambah * 3;
//			awal = 100;
//		}

//		System.out.print("masukkan banyaknya bilangan : ");
//		int bilangan = input.nextInt();
//		
//		int siAngka1=100;
//		for (int i=0; i<bilangan; i++) {
//			int pangkat = (int) Math.pow(3, i);
//			siAngka1=100+pangkat;
//			if (i==0) {
//				siAngka1=100;
//			}
//			System.out.println(siAngka1 + " adalah " + "'Si Angka 1'");
//		}

		// Soal 4 bensin
//		int tempJarak[] = new int[4];
//		double total = 0;
//		double bensin = 0;
//		
//		System.out.println("Masukkan Jarak : ");
//		for (int i = 0; i < tempJarak.length; i++) {
//			int jarak = input.nextInt();
//			tempJarak[i] = jarak;
//		}
//		
//		input.nextLine();
//		
//		System.out.println("Masukkan Index Jarak : ");
//		String inputIndexSplit[] = input.nextLine().split(" "); 
//		
//		for (int i = 0; i < inputIndexSplit.length; i++) {
//			total += tempJarak[Integer.parseInt(inputIndexSplit[i])-1];
//		}
//
//		bensin = total / 2500;
//		System.out.println(bensin + " liter");

		// Soal 5 piring dewasa
//		System.out.println("Laki-laki dewasa = ");
//		int laki = input.nextInt();
//		System.out.println("Prempuan Dewasa = ");
//		int prem = input.nextInt();
//		System.out.println("Anak-anak = ");
//		int anak = input.nextInt();
//		System.out.println("Balita = ");
//		int bal = input.nextInt();
//
//		int total = laki + prem + anak + bal;
//		double porsi = (laki * 2) + prem + (anak / 2) + bal;
//
//		if (total % 2 == 1) {
//			prem += prem;
//			porsi = (laki * 2) + prem + (anak / 2) + bal;
//		} else if(total % 2 == 0) {
//			porsi = (laki * 2) + prem + (anak / 2) + bal;
//		}
//		System.out.println(porsi + " porsi");

		// cara2
//		System.out.println("masukkan jumlah laki-laki dewasa : ");
//		int lakiDewasa = input.nextInt();
//		System.out.println("masukkan jumlah perempuan dewasa : ");
//		int perempuanDewasa = input.nextInt();
//		System.out.println("masukkan jumlah remaja : ");
//		int remaja = input.nextInt();
//		System.out.println("masukkan jumlah anak : ");
//		int anak = input.nextInt();
//		System.out.println("masukkan jumlah balita : ");
//		int balita = input.nextInt();
//
//		double nasgor = 0;
//		int mieAyam = 0;
//		int mie = 0;
//		
//int bubur = 0;
//		int totalOrang = lakiDewasa + perempuanDewasa + remaja + balita;
//		
//		if (totalOrang % 2 != 0 && totalOrang > 5) {
//			perempuanDewasa = perempuanDewasa + 1;
//		}
//		
//		nasgor = nasgor + 2 * lakiDewasa + (double)(anak)/2;
//		mieAyam = mieAyam + remaja;
//		mie = mie + perempuanDewasa;
//		bubur = bubur + balita;
//		int jumlah = (int) Math.ceil(nasgor) + mieAyam + mie + bubur;
//		System.out.println("jumlah porsi nasgor = " + nasgor);
//		System.out.println("jumlah porsi mieAyam = " + mieAyam);

		// Soal 6 rekening bank

//		int password = 123456;
//
//		System.out.print("Masukkan PIN : ");
//		int pin = input.nextInt();
//
//		int saldoakhir = 0;
//
//		if (pin != password) {
//			System.out.println("PIN YANG ANDA MASUKKAN SALAH!");
//		} else {
//			System.out.println("Uang yang disetor : Rp. ");
//			int uang = input.nextInt();
//			System.out.println("Pilihan Transfer : 1. Antar Rekening       2. Antar Bank");
//			int pilihan = input.nextInt();

		// if (String.valueOf(pin).equals("123456")) {
		// System.out.print("Uang yang disetor : Rp. ");
		// int uang = input.nextInt();
		// } else {
		// System.out.println("PIN ANDA SALAH!");
		// }
		// System.out.println("Pilihan Transfer : 1. Antar Rekening 2. Antar Bank");
		// int pilihan = input.nextInt();

//			switch (pilihan) {
//			case 1:
//				System.out.println("Masukkan Rekening Tujuan : ");
//				String rekening = input.nextLine();
//				input.nextLine();
//				
//				if (rekening.length() == 10) {
//					System.out.println("Masukkan Nominal Transfer : ");
//					int transfer = input.nextInt();
//					
//					if (transfer >= uang) {
//						System.out.println("MAAF SALDO ANDA KURANG");
//					} else if (transfer < uang) {
//						saldoakhir = uang - transfer;
//						System.out.println("SISA SALDO ANDA = " + saldoakhir);
//					}
//				} else {
//					System.out.println("SILAHKAN MASUKKAN NOMOR REKENING YANG BENAR");
//				}
//
//				break;
//			case 2:
//				System.out.println("Masukkan kode Bank : ");
//				int kode = input.nextInt();
//				System.out.println("Masukkan Rekening Tujuan : ");
//				int rekTujuan = input.nextInt();
//
//				if (String.valueOf(rekTujuan).equals(10)) {
//					System.out.println("Masukkan Nominal Transfer : ");
//					int transfer1 = input.nextInt();
//					if (transfer1 > uang) {
//						System.out.println("MAAF SALDO ANDA KURANG");
//					} else if (transfer1 < uang) {
//						saldoakhir = (uang - transfer1) - 7500;
//						System.out.println("SISA SALDO ANDA = " + saldoakhir);
//					}
//				} else {
//					System.out.println("SILAHKAN MASUKKAN NOMOR REKENING TUJUAN YANG BENAR");
//				}
//				break;
//			default:
//				System.out.println("ANDA MEMASUKKAN PILIHAN TRANSFER YANG BENAR");
//			}
//		}

//		System.out.println("masukkan password anda :");
//		 
//		int[] pin = { 1, 2, 3, 4 };
//		String hasilPassword = "";
//		int[] tempPassword = new int[4];
//		int saldoAkhir = 0;
//		for (int i = 0; i < tempPassword.length; i++) {
//			int password = input.nextInt();
//			tempPassword[i] = password;
//			{
//				 {
//				if (tempPassword[i] == pin[i]) {
//					hasilPassword = "password anda benar";
//				} else {
//					hasilPassword = "password anda salah";
//				}
// 
//			}
//		}
//		}
//		System.out.println(hasilPassword);
// 
//		if (hasilPassword.equals("password anda benar")) {
// 
//			System.out.println("masukkan jumlah saldo anda :");
//			int saldoAwal = input.nextInt();
// 
//			if (saldoAwal == 0) {
//				System.out.println("Silahkan isi saldo terlebih dahulu ");
//				System.out.println("Masukkan jumlah saldo yang akan di isi : ");
//				int saldoIsi = input.nextInt();
//				saldoAwal = saldoAwal + saldoIsi;
//			}
//			input.nextLine();
//			System.out.println("masukkan pilihan transfer anda : ");
//			System.out.println("---Pilih 1 untuk sesama bank---");
//			System.out.println("---Pilih 2 untuk bank lain--- ");
// 
//			int pilihanTransfer = input.nextInt();
//			input.nextLine();
//			switch (pilihanTransfer) {
//			case 1:
//				System.out.println("masukkan nomer rekening tujuan : ");
//				String noTujuan = input.nextLine();
//				if (noTujuan.length() == 10) {
//					System.out.println("masukkan nominal transfer : ");
//					int nominalTransfer = input.nextInt();
// 
//					if (nominalTransfer >= saldoAwal) {
//						System.out.println("maaf saldo anda kurang !");
//					} else if (nominalTransfer < saldoAwal) {
//						saldoAkhir = saldoAkhir + (saldoAwal - nominalTransfer);
//						System.out.println("saldo anda tersisa sebesar " + saldoAkhir);
//					}
//				} else {
//					System.out.println("silahkan masukkan nomer rekening tujuan yang benar!");
//				}
//				break;
//			case 2:
//				System.out.println("masukkan nomer rekening tujuan (admin Rp. 7500)");
//				String noTujuanLain = input.nextLine();
//				if (noTujuanLain.length() == 10) {
//					System.out.println("masukkan nominal transfer : ");
//					int nominalTransfer = input.nextInt();
//					if (nominalTransfer > saldoAwal) {
//						System.out.println("maaf saldo anda kurang !");
//					} else if (nominalTransfer < saldoAwal) {
//						saldoAkhir = saldoAkhir + (saldoAwal - nominalTransfer - 7500);
//						System.out.println("saldo anda tersisa sebesar " + saldoAkhir);
//					}
//				} else {
//					System.out.println("silahkan masukkan nomer rekening tujuan yang benar! ");
//				}
//				break;
//			default:
//				System.out.println("anda memasukkan pilihan transfer yang salah !");
//			}
//		}

		// Soal 7 kartu atau gambaran
//		System.out.print("Masukan jumlah kartu : ");
//        int kartu = input.nextInt();
//        System.out.print("Jumlah tawaran : ");
//        int tawaran = input.nextInt();
//        System.out.print("Pilih kotak, A atau B : ");
//        String pilih = input.next();
// 
//        Random angka = new Random();
//        int hasilA = 0;
//        int hasilB = 0;
//        int hasilPoint = 0;
// 
//        for (int i = 0; i < 10; i++) {
//            hasilA = angka.nextInt(10);
//        }
//        for (int i = 0; i < 10; i++) {
//            hasilB = angka.nextInt(10);
//        }
// 
//        if (pilih.equals("A") && hasilA > hasilB) {
//            hasilPoint = kartu + tawaran;
//            System.out.println("Kotak A = " + hasilA);
//            System.out.println("Kotak B = " + hasilB);
//            System.out.println("You Win");
//            System.out.println("kartu yang anda punya = " + hasilPoint);
//        } else if (pilih.equals("A") && hasilA < hasilB) {
//            hasilPoint = kartu - tawaran;
//            System.out.println("Kotak A = " + hasilA);
//            System.out.println("Kotak B = " + hasilB);
//            System.out.println("You Lose");
//            System.out.println("kartu yang anda punya = " + hasilPoint);
//        } else if (pilih.equals("B") && hasilA < hasilB) {
//            hasilPoint = kartu + tawaran;
//            System.out.println("Kotak A = " + hasilA);
//            System.out.println("Kotak B = " + hasilB);
//            System.out.println("You Win");
//            System.out.println("kartu yang anda punya = " + hasilPoint);
//        } else if (pilih.equals("B") && hasilA > hasilB) {
//            hasilPoint = kartu - tawaran;
//            System.out.println("Kotak A = " + hasilA);
//            System.out.println("Kotak B = " + hasilB);
//            System.out.println("You Lose");
//            System.out.println("Kartu yang anda punya = " + hasilPoint);
//        }
//        input.close();

		// Soal 8 fibonacci
//		System.out.print("Input : ");
//		int n = input.nextInt();
//		
//		int total[][] = new int[2][n];
//		
//		int x = 0;
//		int y = 1;
//		
//		String genap = "";
//		String ganjil = "";
//		
//		for (int i = 0; i < 2; i++) {
//			for (int j = 0; j < n; j++) {
//				if (i == 0) {
//					total[i][j] = x;
//					genap+= total[i][j] + " ";
//					x = x+2;
//				} else {
//					total[i][j] = y;
//					ganjil += total[i][j] + " ";
//					y += 2;
//				}
//			}
//			System.out.println();
//		}
//		int jumlah = 0;
//		String sum = "";
//		
//		for (int i = 0; i < 1; i++) {
//			for (int j = 0; j < n; j++) {
//				jumlah = total[i][j] + total[i+1][j];
//				sum += jumlah + " ";
//			}
//		}
//		
//		System.out.println("Genap   : " + genap);
//		System.out.println("Ganjil  : " + ganjil);
//		System.out.println("Jumlah  : " + sum);

		// Soal 9 ninjahattori
//		System.out.println("Gunung dan Lembah yang dilewati Hattori :  ");
//		String s = input.nextLine().toUpperCase();
//		
//		char[] huruf = s.toCharArray();
//		int countLembah = 0;
//		int countGunung = 0;
//		
//		for (int i = 0; i < s.length(); i+=2) {
//			if (huruf[i] == 'N' && huruf[i + 1] == 'T') {
//				countGunung++;
//			}else if (huruf[i] == 'T' && huruf[i + 1] == 'N') {
//				countLembah++;
//			}
//		}
//		System.out.println("Banyak Gunung : " + countGunung);
//		System.out.println("Banyak Lembah : " + countLembah);

		// pake substring
//		System.out.println("Gunung dan Lembah yang dilewati Hattori :  ");
//		String s = input.nextLine().toUpperCase();
//		int countLembah = 0;
//		int countGunung = 0;
//		
//		for (int i = 0; i < s.length(); i+=2) {
//			String sub = s.substring(i, i+2);
//			if (sub.equals("NT")) {
//				countGunung++;
//			} if (sub.equals("TN")) {
//				countLembah++;
//			}	
//		}
//		System.out.println("Banyak Gunung : " + countGunung);
//		System.out.println("Banyak Lembah : " + countLembah);

		// Soal 10
		System.out.print("Masukkan saldo OPO = Rp. ");
		int saldo = input.nextInt();

		int hargaDiskon = 0;
		int cashBack = 0;
		int cup = 0;

		if (saldo >= 40000 && saldo <= 100000) {
			cup = saldo / 9000;
			hargaDiskon = cup * 18000 / 2;
			if (hargaDiskon > 100000) {
				hargaDiskon = 100000;
				saldo = (saldo - hargaDiskon);
				cashBack = hargaDiskon * 10 / 100;
				if (cashBack > 30000) {
					cashBack = 30000;
					saldo = saldo + cashBack;
				} else {
					saldo = saldo + cashBack;
				}
			} else {
				saldo = (saldo - hargaDiskon) + (hargaDiskon * 10 / 100);
			}
		} else if (saldo > 100000) {
			saldo = saldo - 100000;
		}
		System.out.println("Jumlah Cup = " + cup + "cup");
		System.out.println("Saldo Akhir = Rp. " + saldo);

	}
}
