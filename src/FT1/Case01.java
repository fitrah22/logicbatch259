package FT1;

import java.text.*;
import java.util.*;

public class Case01 {

	public static void Resolve1() {
		Scanner input = new Scanner(System.in);

		System.out.print("Input : ");
		int n = input.nextInt();

		int total[][] = new int[2][n];

		int x = 0;
		int y = 1;

		String genap = "";
		String ganjil = "";

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					total[i][j] = (3 * j) - 1;
					genap += total[i][j] + " ";
					x = x + 2;
				} else {
					total[i][j] = (-2 * j) * (1);
					ganjil += total[i][j] + " ";
					y += 2;
				}
			}
		}
		int jumlah = 0;
		String sum = "";

		for (int i = 0; i < 1; i++) {
			for (int j = 0; j < n; j++) {
				jumlah = total[i][j] + total[i + 1][j];
				sum += jumlah + " ";
			}
		}

		System.out.println(genap);
		System.out.println(ganjil);
		System.out.println("\nJumlah  : " + sum);
	}

	public static void Resolve2() {
		Scanner input = new Scanner(System.in);

		int tempJarak[] = { 500, 2000, 3500, 5000 };
		int total = 0;
		double waktu = 0;

		System.out.println("Masukkan Index Jarak : ");
		String inputIndexSplit[] = input.nextLine().split(" ");

		for (int i = 1; i < inputIndexSplit.length; i++) {
			total += tempJarak[Integer.parseInt(inputIndexSplit[i]) - 1];
		}

		double totalJarak = total / 1000;
		waktu = ((totalJarak / 30) * 60 + (10 * (inputIndexSplit.length - 1)));

		System.out.println("Total Jarak = " + totalJarak + " km");
		System.out.println(waktu + " menit");

	}

	public static void Resolve3() {
		String Apel[] = new String[99];
		String Jeruk[] = new String[99];
		String Mangga[] = new String[99];
		String Pisang[] = new String[99];

	}

	public static void Resolve4() {
		Scanner input = new Scanner(System.in);

		System.out.println("Uang Andi : ");
		int andi = input.nextInt();

		System.out.println("Jumlah Barang : ");
		int barang = input.nextInt();

		String namaBarang[] = new String[barang];
		int harga[] = new int[barang];

		System.out.println("Nama Barang : ");
		for (int i = 0; i < barang; i++) {
			namaBarang[i] = input.next();
			harga[i] = input.nextInt();
		}

		int belanja = harga[0];
		String beli = "";
		for (int i = 0; i < barang; i++) {
			if (belanja > harga[i]) {
				beli += namaBarang[i] + " ";
				andi -= harga[i];
			}
		}
		System.out.println("Barang yang dibeli = " + beli);
	}

	public static void Resolve5() {
		Scanner input = new Scanner(System.in);

		System.out.print("Input Waktu : ");
		String inputWaktu = input.nextLine();

		String waktu[] = inputWaktu.split(":");
		String amPM = waktu[2].substring(2, 4);

		int jam;
		int menit;

		jam = Integer.parseInt(waktu[0]);
		menit = Integer.parseInt(waktu[0].substring(2, 4));

		String am = "AM";
		String pm = "PM";

		if (amPM.equals(am) && jam == 12) {
			jam += 0;
		} else if (amPM.equals(pm) && jam <= 12) {
			jam += 12;
		} else if (amPM.equals(pm) && jam > 12) {
			jam -= 12;
		}
		System.out.println(jam + ":" + menit);
	}

	public static void Resolve6() {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan Kalimat : ");
		String kalimat = input.nextLine().toLowerCase().replaceAll(" ", "");

		char[] huruf = kalimat.toCharArray();
		String vokal = "";
		String konsonan = "";
		Arrays.sort(huruf);

		for (int i = 0; i < huruf.length; i++) {
			if (huruf[i] == 'a' || huruf[i] == 'i' || huruf[i] == 'u' || huruf[i] == 'e' || huruf[i] == 'o') {
				vokal += huruf[i];
			} else {
				konsonan += huruf[i];
			}
		}
		System.out.println();
		System.out.println("Huruf Vokal : " + vokal);
		System.out.println("Konsonan : " + konsonan);
	}

	public static void Resolve7() {
		Scanner input = new Scanner(System.in);

	}

	public static void Resolve8() {
		Scanner input = new Scanner(System.in);

		System.out.print("Masukkan kalimat : ");
		String kata = input.nextLine();

		String result = "";
		String[] tempSplit = kata.split(" ");

		for (int i = 0; i < tempSplit.length; i++) {
			char[] arrKata = tempSplit[i].toCharArray();
			for (int j = 0; j < 3; j++) {
				if (j == 0) {
					result += arrKata[0];
				} else if (j == 2) {
					result += arrKata[arrKata.length - 1];
				} else {
					result += "***";
				}
			}
			result += " ";
		}
		System.out.println(result);
	}

	public static void Resolve9() {
		Scanner input = new Scanner(System.in);

		System.out.println("String : ");
		String huruf = input.nextLine().toLowerCase();

		char tempArr[] = huruf.toCharArray();
		String hasil = "";

		System.out.println("array : ");
		int angka[] = new int[huruf.length()];
		for (int i = 0; i < angka.length; i++) {
			angka[i] = input.nextInt();
		}

		for (int i = 0; i < angka.length; i++) {
			int ar = (char) (((int) tempArr[i] - 96));
			if (angka[i] == ar) {
				hasil += "True";
			} else {
				hasil += "False";
			}
		}
		System.out.println(hasil);
	}

	public static void Resolve10() {
		Scanner input = new Scanner(System.in);

		System.out.println("Input : ");
		int put = input.nextInt();

		int baju = 0;
		int rok = 0;
		int celana = 0;
		int sepatu = 0;

	}
}
