package dayday;

import java.util.*;

public class TugasMeanMedianModus {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Masukkan jumlah inputan : ");
		int n = input.nextInt();

		int arrInput[] = new int[n];
		int hitung = 0;
		int modus = 0;
		double median = 0;
		double mean = 0;
		double jumlah = 0;
		System.out.println();
		for (int i = 0; i < n; i++) {
			System.out.print("Masukkan data ke " + (i + 1) + " : ");
			arrInput[i] = input.nextInt();
			jumlah += arrInput[i];
		}
		System.out.println("========================================");
		Arrays.sort(arrInput);
		System.out.println("Data yang di Masukkan : " + Arrays.toString(arrInput));

		if (arrInput.length % 2 == 1) {
			median = arrInput[arrInput.length / 2];
		} else {
			median = ((double) arrInput[arrInput.length / 2] + arrInput[(arrInput.length / 2) - 1]) / 2;
		}

		for (int i = 0; i < arrInput.length; i++) {
			int count = 0;
			for (int j = 0; j < arrInput.length; j++) {
				if (arrInput[i] == arrInput[j]) {
					count++;
				} else if (count > hitung) {
					hitung = count;
					modus = arrInput[i];
				}
			}
		}

		mean = jumlah / n;

		System.out.println("\n");
		System.out.println("Nilai Mean   adalah : " + mean);
		System.out.println("Nilai Median adalah : " + median);
		System.out.println("Nilai Modus  adalah : " + modus);

	}
}
