package day3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;

public class PRday3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Soal 1
		System.out.print("Input : ");
		int n = input.nextInt();
		
		int fibo[] = new int[n];
		
		fibo[0] = 1;
		fibo[1] = 1;
		
		for (int i = 2; i < n; i++) {
			fibo[i] = fibo[i - 1] + fibo [i - 2];
		}
		
		for (int i = 0; i < n; i++) {
			System.out.print(fibo[i] + " ");
		}

		// Soal 2
//		System.out.print("Input : ");
//		int  n = input.nextInt();
//		
//		int fibo[] = new int[n];
//		
//		fibo[0] = 1;
//		fibo[1] = 1;
//		fibo[2] = 1;
//		
//		for (int i = 3; i < n; i++) {
//			fibo[i] = fibo[i - 1] + fibo [i - 2] + fibo [i - 3];
//		}
//		
//		for (int i = 0; i < n; i++) {
//			System.out.print(fibo[i] + " ");
//		}

		// Soal 3
//		System.out.print("Input : ");
//		int n = input.nextInt();
//		
//		for (int i = 0; i < n; i++) {
//			int prima = 0;
//			for (int j = 1; j < n; j++) {
//				if (i % j == 0) {
//					prima = prima + 1;
//				}
//			}	
//			if (prima == 2) {
//				System.out.print(i + " ");
//			}
//		}

		// Soal 4
//		System.out.println(date.toString());

		System.out.print("Input Waktu : ");
		String waktu = input.nextLine().toLowerCase();

		DateFormat awal = new SimpleDateFormat("hh:mm:ssaa");
		DateFormat akhir = new SimpleDateFormat("HH:mm:ss");

		try {
			Date date = awal.parse(waktu);
			String out = akhir.format(date);
			System.out.println(out);
		} catch (ParseException p) {
			p.printStackTrace();
		}

		// Soal 5
		System.out.print("Input : ");
		int pohon = input.nextInt();

		int faktor = 0;
		
		for (int i = 2; i <= pohon; i++) {
			for (int j = 1; j <= i; j++) {
				if (pohon % 1 == 0) {
					faktor = pohon / i;
					System.out.println(pohon + "/" + i + " = " + faktor);
					pohon = faktor;
				} 	
			}
		}
		

	}

}
